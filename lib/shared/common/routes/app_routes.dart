class AppRoutes {
  static const String onBoarding = '/on_boarding';
  static const String initialPage = '/initial_page';
  static const String loginPage = '/login';
  static const String homePage = '/home';
}
