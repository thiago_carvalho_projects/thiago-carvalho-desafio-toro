import 'package:desafio_toro/modules/default/view/default_not_found_page.dart';
import 'package:desafio_toro/modules/home/home_page.dart';
import 'package:desafio_toro/modules/login/login_page.dart';
import 'package:desafio_toro/modules/on_boarding/on_boarding_page.dart';
import 'package:desafio_toro/toro_splash_screen.dart';
import 'package:flutter/material.dart';

import 'app_routes.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings settings) {
    switch (settings.name) {
      case AppRoutes.initialPage:
        return MaterialPageRoute(builder: (_) => ToroSplashScreen());

      case AppRoutes.onBoarding:
        return MaterialPageRoute(builder: (_) => OnBoardingPage());

      case AppRoutes.loginPage:
        return MaterialPageRoute(builder: (_) => LoginPage());

      case AppRoutes.homePage:
        return MaterialPageRoute(builder: (_) => HomePage());

      default:
        return MaterialPageRoute(builder: (_) => DefaultNotFoundPage());
    }
  }
}
