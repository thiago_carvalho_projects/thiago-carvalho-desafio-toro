import 'package:rxdart/rxdart.dart';

/// Controla o fluxo de iniciar e finalizar o loading.
/// caso a operação falhe, pode emitir erro.
class LoadingController {
  final BehaviorSubject<bool> _loadingController = BehaviorSubject<bool>();

  Stream get stream => _loadingController.stream;

  void startLoading() {
    _loadingController.sink.add(true);
  }

  Future<void> endLoading() async {
    _loadingController.sink.add(false);
    await Future.delayed(Duration(milliseconds: 200));
  }

  void emitError(String error) {
    _loadingController.sink.addError(error);
  }

  dispose() {
    _loadingController.close();
  }
}
