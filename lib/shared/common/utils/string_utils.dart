class StringUtils {
  static bool isNumeric(String str) {
    if (str == null) {
      return false;
    }
    return double.tryParse(str) != null;
  }

  static String onlyNumbers(String str) {
    RegExp regex = new RegExp(r"[^0-9]", caseSensitive: false, multiLine: true);

    return str.replaceAll(regex, "");
  }
}
