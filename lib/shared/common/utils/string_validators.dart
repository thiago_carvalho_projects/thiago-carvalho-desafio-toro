import 'package:desafio_toro/shared/common/utils/valida_cpf.dart';

extension StringValidatos on String {
  String validateEmail() {
    if (!RegExp(
            r"^[a-zA-Z0-9.a-zA-Z0-9.!#$%&'*+-/=?^_`{|}~]+@[a-zA-Z0-9]+\.[a-zA-Z]+")
        .hasMatch(this)) {
      return "O email digitado não é válido";
    }
    return null;
  }

  String validatePassword() {
    if (this.length < 6) {
      return "A senha é muito curta";
    }
    return null;
  }

  String validateCPF() {
    if (!ValidaCPF().validarCPF(this)) return "O CPF digitado não é valido";

    return null;
  }
}
