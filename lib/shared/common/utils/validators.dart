import 'dart:async';

import 'package:desafio_toro/shared/common/utils/string_utils.dart';

import 'string_validators.dart';

class Validators {
  static final StreamTransformer<String, String> validateLogin =
      StreamTransformer<String, String>.fromHandlers(handleData: (login, sink) {
    String validationMessage;

    if (StringUtils.isNumeric(login)) {
      validationMessage = login.validateCPF();
    } else {
      validationMessage = login.validateEmail();
    }
    if (validationMessage == null) {
      sink.add(login);
    } else {
      sink.addError(validationMessage);
    }
  });

  static final validatePassword =
      StreamTransformer<String, String>.fromHandlers(
          handleData: (password, sink) {
    if (password.validatePassword() == null)
      sink.add(password);
    else
      sink.addError(password.validatePassword());
  });
}
