class ComponentSizes {
  /// 4 pixels spacing
  static const double SPACE_XXS = 4.0;

  /// 8 pixels spacing
  static const double SPACE_XS = 8.0;

  /// 16 pixels spacing
  static const double SPACE_SM = 16.0;

  /// 32 pixels spacing
  static const double SPACE_MD = 32.0;

  /// 48 pixels spacing
  static const double SPACE_LG = 48.0;

  /// 72 pixels spacing
  static const double SPACE_XL = 72.0;
}
