import 'package:flutter/painting.dart';

class ToroColors {
  static const Color primaryColor = Color(0xFF33BCD4);
  static const Color blueDarkest = Color(0xFF091927);
}
