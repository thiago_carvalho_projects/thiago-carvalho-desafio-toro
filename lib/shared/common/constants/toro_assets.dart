class ToroAssets {
  static const String erro = 'assets/images/Erro.svg';
  static const String logoIcon = 'assets/images/logo-icon.png';
  static const String logoIconWhite = 'assets/images/logo-icon-white.png';
  static const String logoTransparent = 'assets/images/logo-transparent.png';
}
