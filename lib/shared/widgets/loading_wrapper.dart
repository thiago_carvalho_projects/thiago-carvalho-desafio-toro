import 'package:desafio_toro/shared/common/utils/loading_controller.dart';
import 'package:desafio_toro/shared/widgets/toro_default_loading_indicator.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

/// Qualquer widget filho poderá mostrar o loading

/// Mostra o loading indicator caso o loadingController emita true
/// Mostra o child caso loadingController emita false.
class LoadingWrapper extends StatefulWidget {
  final LoadingController loadingController;
  final Widget child;
  final Widget loadingProgressIndicator;
  final Widget dialogProgressIndicator;
  final Widget errorIndicator;

  const LoadingWrapper({
    Key key,
    @required this.loadingController,
    @required this.child,
    this.loadingProgressIndicator,
    this.dialogProgressIndicator,
    this.errorIndicator,
  }) : super(key: key);

  @override
  _LoadingWrapperState createState() => _LoadingWrapperState();
}

class _LoadingWrapperState extends State<LoadingWrapper> {
  @override
  Widget build(BuildContext context) {
    return StreamBuilder<bool>(
        initialData: false,
        stream: widget.loadingController.stream,
        builder: (context, snapshot) {
          if (snapshot.hasError)
            return widget.errorIndicator ?? widget.child;
          else if (snapshot.hasData && snapshot.data) {
            return Center(
                child: widget.loadingProgressIndicator ??
                    DefaultToroLoadingIndicator());
          }
          return widget.child;
        });
  }
}
