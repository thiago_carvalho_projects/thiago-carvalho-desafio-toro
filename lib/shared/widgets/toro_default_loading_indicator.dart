import 'dart:async';

import 'package:desafio_toro/shared/common/constants/toro_assets.dart';
import 'package:flutter/material.dart';

/// Loading default
class DefaultToroLoadingIndicator extends StatefulWidget {
  @override
  _DefaultToroLoadingIndicatorState createState() =>
      _DefaultToroLoadingIndicatorState();
}

class _DefaultToroLoadingIndicatorState
    extends State<DefaultToroLoadingIndicator> {
  double width = 0;
  double height = 0;

  bool isPrimaryColor = true;

  Color selectedColor = Colors.white;

  final StreamController<Size> controllerContainerSize =
      StreamController<Size>();

  animate() {
    if (isPrimaryColor) {
      selectedColor = Color(0xFF33BCD4);
    } else {
      selectedColor = Colors.white;
    }

    isPrimaryColor = !isPrimaryColor;

    controllerContainerSize.sink.add(MediaQuery.of(context).size);
  }

  @override
  void dispose() {
    controllerContainerSize.close();
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      animate();
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Center(
      child: StreamBuilder<Size>(
          stream: controllerContainerSize.stream,
          builder: (context, snapshot) {
            if (snapshot.hasData) {
              width = snapshot.data.width;
              height = snapshot.data.height;
            }

            return Center(
              child: AnimatedContainer(
                  onEnd: () {
                    setState(() {
                      animate();
                    });
                  },
                  alignment: Alignment.center,
                  width: 60,
                  height: 60,
                  curve: Curves.easeInQuart,
                  child: Padding(
                    padding: const EdgeInsets.all(12.0),
                    child: AnimatedSwitcher(
                        duration: Duration(milliseconds: 2000),
                        child: isPrimaryColor
                            ? Image.asset(
                                ToroAssets.logoIcon,
                              )
                            : Image.asset(
                                ToroAssets.logoIconWhite,
                                key: ValueKey('secondary'),
                              )),
                  ),
                  decoration: BoxDecoration(
                      color: selectedColor ?? Colors.white,
                      shape: BoxShape.circle),
                  duration: Duration(milliseconds: 2000)),
            );
          }),
    );
  }
}
