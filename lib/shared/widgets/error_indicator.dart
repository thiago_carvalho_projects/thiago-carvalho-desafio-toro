import 'package:flutter/material.dart';

class DefaultErrorIndicator extends StatelessWidget {
  final String error;

  const DefaultErrorIndicator({Key key, this.error}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return AlertDialog(
      actions: [
        FlatButton(
          onPressed: () => Navigator.pop(context),
          child: Text('fechar'),
        ),
      ],
      title: Text(
        error ?? 'Ocorreu um erro',
        textAlign: TextAlign.center,
      ),
    );
  }
}
