import 'package:desafio_toro/shared/common/utils/loading_controller.dart';
import 'package:desafio_toro/shared/widgets/toro_default_loading_indicator.dart';
import 'package:flutter/material.dart';

import 'error_indicator.dart';

/// Mostra o dialog com quando `loadingController` emite true
/// Mostra erro caso `loadingController` emita um erro
class LoadingDialogListener extends StatefulWidget {
  final LoadingController loadingController;

  /// Widget que será mostrado no dialog quando carregando
  /// por default será `DefaultToroLoadingIndicator`
  final Widget dialogProgressIndicator;

  /// Widget que será mostrado no dialog quando emitido um erro
  /// por default será `DefaultErrorIndicator`
  final Widget errorIndicator;

  /// widget que irá escutar o `lodingController`
  final Widget child;

  final bool barrierDismissible;

  const LoadingDialogListener(
      {Key key,
      this.loadingController,
      this.barrierDismissible = true,
      this.dialogProgressIndicator,
      this.errorIndicator,
      this.child})
      : super(key: key);

  @override
  _LoadingListenerState createState() => _LoadingListenerState();
}

class _LoadingListenerState extends State<LoadingDialogListener> {
  BuildContext errorContext;
  BuildContext loadingContext;

  @override
  void initState() {
    widget.loadingController.stream.listen((event) {
      if (errorContext != null && Navigator.canPop(errorContext)) {
        Navigator.pop(errorContext);
      }

      if (event)
        showDialog(
            barrierDismissible: widget.barrierDismissible,
            context: context,
            builder: (ctx) {
              loadingContext = ctx;
              return widget.dialogProgressIndicator ??
                  DefaultToroLoadingIndicator();
            }).then((value) => loadingContext = null);
      else {
        Navigator.maybePop(loadingContext);
      }
    }, onError: (error) {
      if (loadingContext != null && Navigator.canPop(loadingContext))
        Navigator.pop(loadingContext);

      showDialog(
          barrierDismissible: widget.barrierDismissible,
          context: context,
          builder: (context) {
            errorContext = context;
            return widget.errorIndicator ??
                DefaultErrorIndicator(
                  error: error,
                );
          }).then((value) => errorContext = null);
    });

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return widget.child;
  }
}
