import 'package:desafio_toro/shared/common/constants/component_sizes.dart';
import 'package:flutter/material.dart';

/// Contains useful consts to reduce boilerplate and duplicate code
class ComponentSpacer {
  static const Widget verticalSpaceXXS =
      SizedBox(height: ComponentSizes.SPACE_XXS);
  static const Widget verticalSpaceXS =
      SizedBox(height: ComponentSizes.SPACE_XS);
  static const Widget verticalSpaceSM =
      SizedBox(height: ComponentSizes.SPACE_SM);
  static const Widget verticalSpaceMD =
      SizedBox(height: ComponentSizes.SPACE_MD);
  static const Widget verticalSpaceLG =
      SizedBox(height: ComponentSizes.SPACE_LG);

  static const Widget horizontalSpaceXXS =
      SizedBox(width: ComponentSizes.SPACE_XXS);
  static const Widget horizontalSpaceXS =
      SizedBox(width: ComponentSizes.SPACE_XS);
  static const Widget horizontalSpaceSM =
      SizedBox(width: ComponentSizes.SPACE_SM);
  static const Widget horizontalSpaceMD =
      SizedBox(width: ComponentSizes.SPACE_MD);
  static const Widget horizontalSpaceLG =
      SizedBox(width: ComponentSizes.SPACE_LG);
}
