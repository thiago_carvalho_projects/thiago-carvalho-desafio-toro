import 'package:desafio_toro/shared/common/constants/toro_colors.dart';
import 'package:flutter/material.dart';

/// Estilos
enum ToroButtonStyle { CONTAINED, OUTLINED }

class ToroButton extends StatelessWidget {
  final ToroButtonStyle style;
  final Widget child;
  final Color buttonColor;
  final Function onPressed;
  final double buttonHeight;
  final double buttonWidth;

  const ToroButton(
      {Key key,
      this.style = ToroButtonStyle.CONTAINED,
      this.child,
      this.onPressed,
      this.buttonColor,
      this.buttonHeight = 50,
      this.buttonWidth = double.maxFinite})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: buttonHeight,
      width: buttonWidth,
      child: style == ToroButtonStyle.CONTAINED
          ? RaisedButton(
              child: child,
              disabledColor: Colors.grey,
              color: buttonColor ?? ToroColors.primaryColor,
              onPressed: onPressed,
            )
          : OutlineButton(
              child: child,
              borderSide: BorderSide(color: buttonColor),
              onPressed: onPressed),
    );
  }
}
