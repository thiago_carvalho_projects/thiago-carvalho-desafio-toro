import 'dart:async';

import 'package:desafio_toro/shared/common/constants/toro_assets.dart';
import 'package:desafio_toro/shared/common/constants/toro_colors.dart';
import 'package:desafio_toro/shared/common/routes/app_routes.dart';
import 'package:desafio_toro/toro_desafio.dart';
import 'package:flutter/material.dart';

class ToroSplashScreen extends StatefulWidget {
  @override
  _ToroSplashScreenState createState() => _ToroSplashScreenState();
}

class _ToroSplashScreenState extends State<ToroSplashScreen> {
  bool showPrimaryImage = true;

  double imageSize = 10;

  Color color = ToroColors.primaryColor;

  animateBackground() {
    setState(() {
      color = Colors.white;
    });
  }

  animateImage() {
    setState(() {
      showPrimaryImage = false;

      imageSize = 120;
    });
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void didChangeDependencies() {
    WidgetsBinding.instance.addPostFrameCallback((timeStamp) {
      setState(() {
        animateBackground();
        animateImage();
      });
    });

    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Center(
      child: AnimatedContainer(
          alignment: Alignment.center,
          width: double.maxFinite,
          height: double.maxFinite,
          curve: Curves.easeInBack,
          child: AnimatedContainer(
            onEnd: () => Future.delayed(Duration(milliseconds: 1000)).then(
              (value) => navigator.currentState.pushNamed(AppRoutes.onBoarding),
            ),
            height: imageSize,
            width: imageSize,
            curve: Curves.fastLinearToSlowEaseIn,
            duration: Duration(milliseconds: 3000),
            child: AnimatedSwitcher(
                duration: Duration(milliseconds: 1000),
                child: !showPrimaryImage
                    ? Image.asset(
                        ToroAssets.logoIcon,
                      )
                    : Image.asset(
                        ToroAssets.logoIconWhite,
                        key: ValueKey('secondary'),
                      )),
          ),
          decoration: BoxDecoration(
            color: color,
          ),
          duration: Duration(milliseconds: 2000)),
    ));
  }
}
