import 'package:desafio_toro/shared/common/constants/toro_assets.dart';
import 'package:desafio_toro/shared/widgets/component_spacer.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';

class DefaultNotFoundPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: [
            Text(
              'Opss... página ainda em construção!',
              textAlign: TextAlign.center,
              style: TextStyle(fontSize: 26),
            ),
            ComponentSpacer.verticalSpaceMD,
            SvgPicture.asset(
              ToroAssets.erro,
            ),
          ],
        ),
      ),
    );
  }
}
