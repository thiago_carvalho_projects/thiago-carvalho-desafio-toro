import 'package:desafio_toro/modules/login/controller/login_controller.dart';
import 'package:desafio_toro/shared/common/utils/loading_controller.dart';
import 'package:desafio_toro/shared/widgets/loading_listener.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'view/login_view.dart';

class LoginPage extends StatelessWidget {
  final LoadingController loadingController = LoadingController();

  @override
  Widget build(BuildContext context) {
    return Provider(
      create: (context) => LoginController(loadingController),
      child: LoadingDialogListener(
          loadingController: loadingController, child: LoginView()),
    );
  }
}
