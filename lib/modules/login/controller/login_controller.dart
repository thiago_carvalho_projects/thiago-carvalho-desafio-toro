import 'dart:async';

import 'package:desafio_toro/shared/common/routes/app_routes.dart';
import 'package:desafio_toro/shared/common/utils/loading_controller.dart';
import 'package:desafio_toro/shared/common/utils/validators.dart';
import 'package:rxdart/rxdart.dart';

import '../../../toro_desafio.dart';

class LoginController {
  BehaviorSubject<String> emailController = BehaviorSubject<String>();
  BehaviorSubject<String> passwordController = BehaviorSubject<String>();
  final LoadingController loadingController;

  LoginController(this.loadingController);

  Stream<String> get email =>
      emailController.stream.transform(Validators.validateLogin);

  Stream<String> get password =>
      passwordController.stream.transform(Validators.validatePassword);

  Stream get validLogin => Rx.combineLatest2(email, password, (a, b) => true);

  login() async {
    // loadingController.startLoading();

    // loadingController.endLoading();

    navigator.currentState.pushNamed(AppRoutes.homePage);
  }

  dispose() {
    emailController.close();
    passwordController.close();
  }
}
