import 'package:desafio_toro/shared/common/constants/font_family.dart';
import 'package:desafio_toro/shared/common/constants/toro_assets.dart';
import 'package:desafio_toro/shared/common/constants/toro_colors.dart';
import 'package:desafio_toro/shared/widgets/component_spacer.dart';
import 'package:desafio_toro/shared/widgets/toro_button.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import '../controller/login_controller.dart';

class LoginView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final LoginController loginController =
        Provider.of<LoginController>(context);

    MediaQuery.of(context).size.height;

    return Scaffold(
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height,
          child: Column(
            children: [
              Padding(
                padding: const EdgeInsets.all(24),
                child: Column(
                  children: [
                    ComponentSpacer.verticalSpaceMD,
                    Align(
                      alignment: Alignment.centerLeft,
                      child: Image.asset(
                        ToroAssets.logoTransparent,
                        height: 20,
                      ),
                    ),
                    ComponentSpacer.verticalSpaceSM,
                    Align(
                      alignment: Alignment.centerLeft,
                      child: RichText(
                          textAlign: TextAlign.start,
                          text: TextSpan(children: [
                            TextSpan(
                                text: 'O jeito mais fácil de ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 32,
                                    fontFamily: fontFamily,
                                    color: Colors.black)),
                            TextSpan(
                                text: 'investir ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 32,
                                    fontFamily: fontFamily,
                                    color: ToroColors.primaryColor)),
                            TextSpan(
                                text: 'na Bolsa. ',
                                style: TextStyle(
                                    fontWeight: FontWeight.bold,
                                    fontSize: 32,
                                    fontFamily: fontFamily,
                                    color: Colors.black)),
                          ])),
                    ),
                    ComponentSpacer.verticalSpaceMD,
                    Align(
                      alignment: Alignment.center,
                      child: Column(
                        children: [
                          StreamBuilder<String>(
                              stream: loginController.email,
                              builder: (context, snapshot) {
                                return TextField(
                                  onChanged:
                                      loginController.emailController.sink.add,
                                  decoration: InputDecoration(
                                      errorText: snapshot.error,
                                      labelText: 'E-mail ou CPF:',
                                      labelStyle: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500)),
                                );
                              }),
                          StreamBuilder<String>(
                              stream: loginController.password,
                              builder: (context, snapshot) {
                                return TextField(
                                  obscureText: true,
                                  onChanged: loginController
                                      .passwordController.sink.add,
                                  decoration: InputDecoration(
                                      labelText: 'Senha:',
                                      errorText: snapshot.error,
                                      labelStyle: TextStyle(
                                          color: Colors.black,
                                          fontWeight: FontWeight.w500)),
                                );
                              }),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Spacer(),
              Container(
                color: ToroColors.blueDarkest,
                child: Padding(
                  padding:
                      const EdgeInsets.symmetric(horizontal: 16, vertical: 32),
                  child: StreamBuilder(
                      stream: loginController.validLogin,
                      builder: (context, snapshot) {
                        return ToroButton(
                          onPressed: snapshot.hasData
                              ? () {
                                  // showDialog(
                                  //     // useRootNavigator: true,
                                  //     context: context,
                                  //     builder: (context) {
                                  //       return DefaultToroLoadingIndicator();
                                  //     });
                                  loginController.login();
                                }
                              : null,
                          child: Text(
                            'ENTRAR',
                            style: TextStyle(color: Colors.white, fontSize: 16),
                          ),
                        );
                      }),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
