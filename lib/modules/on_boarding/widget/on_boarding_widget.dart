import 'package:desafio_toro/modules/on_boarding/widget/step_widget.dart';
import 'package:desafio_toro/shared/widgets/component_spacer.dart';
import 'package:flutter/material.dart';

enum ImageAlign { TOP, BOTTOM }

class OnBoardingWidget extends StatelessWidget {
  final Widget image;
  final Widget title;
  final Widget subtitle;
  final Widget body;
  final String subtitleText;
  final String titleText;
  final int currentStep;
  final int totalSteps;
  final ImageAlign imageAlign;

  const OnBoardingWidget(
      {Key key,
      this.image,
      this.title,
      this.subtitle,
      this.body,
      this.subtitleText,
      this.titleText,
      this.currentStep,
      this.totalSteps,
      this.imageAlign = ImageAlign.TOP})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.all(24),
      child: Column(
        children: [
          Expanded(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                if (image != null && imageAlign == ImageAlign.TOP)
                  Flexible(
                    child: Padding(
                      padding: const EdgeInsets.only(bottom: 32),
                      child: image,
                    ),
                  ),
                title != null
                    ? title
                    : titleText != null
                        ? Padding(
                            padding: const EdgeInsets.only(bottom: 32),
                            child: Text(
                              titleText,
                              textAlign: TextAlign.center,
                              style: TextStyle(fontSize: 26),
                            ),
                          )
                        : Container(),
                subtitle != null
                    ? subtitle
                    : subtitleText != null
                        ? Text(
                            subtitleText,
                            style: TextStyle(fontSize: 16),
                            textAlign: TextAlign.center,
                          )
                        : Container(),
                if (body != null) body,
                if (image != null && imageAlign == ImageAlign.BOTTOM)
                  Flexible(child: image),
              ],
            ),
          ),
          ComponentSpacer.verticalSpaceSM,
          StepWidget(
            activeIndex: currentStep,
            length: totalSteps,
          )
        ],
      ),
    );
  }
}
