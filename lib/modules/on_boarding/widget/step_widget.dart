import 'package:desafio_toro/shared/common/constants/toro_colors.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class StepWidget extends StatefulWidget {
  final int length;
  final int activeIndex;
  final Function onTap;

  StepWidget({this.length, this.activeIndex, this.onTap});

  @override
  _StepWidgetState createState() => _StepWidgetState();
}

class _StepWidgetState extends State<StepWidget>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final List fakeList = new List(widget.length);

    return Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: fakeList.asMap().entries.map((e) {
        return Container(
          margin: const EdgeInsets.only(right: 8.0),
          width: 10,
          height: 10,
          decoration: BoxDecoration(
            color: e.key == widget.activeIndex
                ? ToroColors.primaryColor
                : Colors.grey,
            shape: BoxShape.circle,
          ),
        );
      }).toList(),
    );
  }
}
