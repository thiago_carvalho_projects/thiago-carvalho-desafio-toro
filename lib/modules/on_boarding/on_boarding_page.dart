import 'package:desafio_toro/modules/on_boarding/controller/on_boarding_controller.dart';
import 'package:desafio_toro/modules/on_boarding/repositories/impl/on_boarding_repository_impl.dart';
import 'package:desafio_toro/modules/on_boarding/services/impl/on_boarding_services_impl.dart';
import 'package:desafio_toro/modules/on_boarding/view/on_boarding_view.dart';
import 'package:desafio_toro/toro_desafio.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class OnBoardingPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Provider(
        child: OnBoardingView(),
        create: (context) => OnBoardingController(
            navigator: navigator,
            repository: OnBoardingRepositoryImpl(OnBoardingServicesImpl())));
  }
}
