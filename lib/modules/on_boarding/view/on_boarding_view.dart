import 'package:desafio_toro/modules/on_boarding/controller/on_boarding_controller.dart';
import 'package:desafio_toro/modules/on_boarding/model/on_boarding_model.dart';
import 'package:desafio_toro/modules/on_boarding/widget/on_boarding_widget.dart';
import 'package:desafio_toro/shared/common/constants/component_sizes.dart';
import 'package:desafio_toro/shared/common/constants/toro_colors.dart';
import 'package:desafio_toro/shared/widgets/component_spacer.dart';
import 'package:desafio_toro/shared/widgets/loading_wrapper.dart';
import 'package:desafio_toro/shared/widgets/toro_button.dart';
import 'package:desafio_toro/toro_desafio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/svg.dart';
import 'package:provider/provider.dart';

class OnBoardingView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final OnBoardingController controller =
        Provider.of<OnBoardingController>(context);

    controller.getOnboarding();

    return Scaffold(
      body: Center(
        child: LoadingWrapper(
          loadingController: controller.loadingController,
          child: Padding(
            padding: const EdgeInsets.only(top: 24),
            child: Column(
              children: [
                Flexible(
                  child: StreamBuilder<List<OnBoardingModel>>(
                      stream: controller.onBoardingController.stream,
                      builder: (context, snapshot) {
                        final List<OnBoardingModel> onBoarding = snapshot.data;

                        if (onBoarding == null)
                          return Container(
                            height: MediaQuery.of(context).size.height,
                          );

                        return SizedBox(
                          child: PageView(
                              children:
                                  List.generate(onBoarding.length, (int index) {
                            OnBoardingModel model = onBoarding[index];

                            return OnBoardingWidget(
                                imageAlign: model.subTitle != null ||
                                        model.options != null
                                    ? ImageAlign.TOP
                                    : ImageAlign.BOTTOM,
                                totalSteps: onBoarding.length,
                                currentStep: index,
                                titleText: model.title,
                                subtitleText: model.subTitle,
                                body: model.options != null &&
                                        model.options.isNotEmpty
                                    ? _buildBody(model)
                                    : null,
                                image: SvgPicture.asset(
                                  model.imageUrl,
                                ));
                          })),
                        );
                      }),
                ),
                Padding(
                  padding: const EdgeInsets.all(ComponentSizes.SPACE_SM),
                  child: Column(
                    children: [
                      ToroButton(
                        buttonColor: ToroColors.primaryColor,
                        onPressed: () {
                          navigator.currentState.pushNamed('sign_up');
                        },
                        child: Text(
                          'Abra sua conta grátias',
                          style: TextStyle(color: Colors.white),
                        ),
                      ),
                      ComponentSpacer.verticalSpaceSM,
                      ToroButton(
                        style: ToroButtonStyle.OUTLINED,
                        buttonColor: ToroColors.primaryColor,
                        onPressed: () {
                          controller.goToLoginPage();
                        },
                        child: Text(
                          'Entrar',
                          style: TextStyle(color: ToroColors.primaryColor),
                        ),
                      )
                    ],
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildBody(OnBoardingModel onBoarding) {
    return Column(
        children: onBoarding.options
            .map((e) => Padding(
                  padding: const EdgeInsets.only(bottom: 16),
                  child: Row(
                    children: [
                      Icon(
                        Icons.check,
                        size: 16,
                        color: ToroColors.primaryColor,
                      ),
                      Flexible(
                        child: Padding(
                          padding: const EdgeInsets.only(left: 8),
                          child: Text(
                            e,
                            style: TextStyle(fontSize: 16),
                          ),
                        ),
                      )
                    ],
                  ),
                ))
            .toList());
  }
}
