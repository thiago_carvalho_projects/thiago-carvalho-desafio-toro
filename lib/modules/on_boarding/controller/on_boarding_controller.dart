import 'dart:async';

import 'package:desafio_toro/modules/on_boarding/model/on_boarding_model.dart';
import 'package:desafio_toro/modules/on_boarding/repositories/on_boarding_repository.dart';
import 'package:desafio_toro/modules/on_boarding/useCases/get_on_boarding_use_case.dart';
import 'package:desafio_toro/shared/common/routes/app_routes.dart';
import 'package:desafio_toro/shared/common/utils/loading_controller.dart';
import 'package:flutter/material.dart';
import 'package:rxdart/subjects.dart';

class OnBoardingController {
  final OnBoardingRepository repository;
  final GlobalKey<NavigatorState> navigator;

  final BehaviorSubject<List<OnBoardingModel>> onBoardingController =
      BehaviorSubject<List<OnBoardingModel>>();

  final LoadingController loadingController = LoadingController();

  OnBoardingController({this.repository, this.navigator});

  Future<void> getOnboarding() async {
    try {
      loadingController.startLoading();

      // Apenas para simular o loading
      await Future.delayed(Duration(seconds: 3));

      List<OnBoardingModel> onBoardingPages =
          await GetOnBoardingUseCase(this.repository)();

      loadingController.endLoading();

      onBoardingController.sink.add(onBoardingPages);
    } catch (e) {
      loadingController.emitError('Ocorreu um erro ao carregar o On boarding');
    }
  }

  goToLoginPage() {
    navigator.currentState.pushNamed(AppRoutes.loginPage);
  }

  dispose() {
    onBoardingController.close();
    loadingController.dispose();
  }
}
