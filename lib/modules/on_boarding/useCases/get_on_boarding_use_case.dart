import 'package:desafio_toro/modules/on_boarding/model/on_boarding_model.dart';
import 'package:desafio_toro/modules/on_boarding/repositories/on_boarding_repository.dart';

class GetOnBoardingUseCase {
  final OnBoardingRepository repository;

  GetOnBoardingUseCase(this.repository);

  Future<List<OnBoardingModel>> call() {
    return repository.getOnboarding();
  }
}
