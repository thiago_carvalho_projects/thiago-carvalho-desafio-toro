import 'package:http/http.dart';

abstract class OnBoardingServices {
  Future<Response> getOnboarding();
}
