import 'package:http/http.dart';

import '../on_boarding_services.dart';

class OnBoardingServicesImpl implements OnBoardingServices {
  @override
  Future<Response> getOnboarding() async {
    return Future.value(
      Response(json, 200),
    );
  }
}

const String json = """
[
  {
    "title": "Olá\\nAgora você tem o jeito mais fácil de investir na Bolsa.",
    "imageUrl": "assets/images/onboarding/step1.svg"
  },
  {
    "title": "Corretagem Zero",
    "subTitle": "Aproveite para investir com Corretagem Zero em qualquer tipo de ativo, inclusive da Bolsa.",
    "imageUrl": "assets/images/onboarding/step2.svg"
  }, 
  {
    "title": "Cashback em Fundos de Investimentos",
    "subTitle": "Receba parte da taxa e administração, em dinheiro, direto na sua conta Toro.",
    "imageUrl": "assets/images/onboarding/step3.svg"
  },
  {
    "title": "E tem muito mais!",
    "imageUrl": "assets/images/onboarding/step4.svg",
    "options": [
      "Recomendações de investimentos.",
      "Cursos do iniciante ao avançado.",
      "Invista sabendo quanto pode ganhar."
    ]
  }
]
""";
