import 'package:json_annotation/json_annotation.dart';

part 'on_boarding_model.g.dart';

@JsonSerializable()
class OnBoardingModel {
  String title;
  String subTitle;
  String imageUrl;
  List<String> options;
  int currentStep;

  OnBoardingModel(
      {this.title,
      this.subTitle,
      this.imageUrl,
      this.options,
      this.currentStep});

  factory OnBoardingModel.fromJson(Map<String, dynamic> json) =>
      _$OnBoardingModelFromJson(json);

  Map<String, dynamic> toJson() => _$OnBoardingModelToJson(this);
}
