// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'on_boarding_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

OnBoardingModel _$OnBoardingModelFromJson(Map<String, dynamic> json) {
  return OnBoardingModel()
    ..title = json['title'] as String
    ..subTitle = json['subTitle'] as String
    ..imageUrl = json['imageUrl'] as String
    ..options = (json['options'] as List)?.map((e) => e as String)?.toList()
    ..currentStep = json['currentStep'] as int;
}

Map<String, dynamic> _$OnBoardingModelToJson(OnBoardingModel instance) =>
    <String, dynamic>{
      'title': instance.title,
      'subTitle': instance.subTitle,
      'imageUrl': instance.imageUrl,
      'options': instance.options,
      'currentStep': instance.currentStep,
    };
