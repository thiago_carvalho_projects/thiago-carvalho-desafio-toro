import 'dart:convert';

import 'package:desafio_toro/modules/on_boarding/model/on_boarding_model.dart';
import 'package:desafio_toro/modules/on_boarding/services/on_boarding_services.dart';
import 'package:http/http.dart';

import '../on_boarding_repository.dart';

class OnBoardingRepositoryImpl implements OnBoardingRepository {
  final OnBoardingServices services;

  OnBoardingRepositoryImpl(this.services);

  @override
  Future<List<OnBoardingModel>> getOnboarding() async {
    Response response = await services.getOnboarding();

    List list = jsonDecode(response.body) as List;

    return list.map((e) => OnBoardingModel.fromJson(e)).toList();
  }
}
