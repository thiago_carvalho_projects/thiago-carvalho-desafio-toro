import 'package:desafio_toro/modules/on_boarding/model/on_boarding_model.dart';

abstract class OnBoardingRepository {
  Future<List<OnBoardingModel>> getOnboarding();
}
