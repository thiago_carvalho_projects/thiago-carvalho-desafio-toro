import 'package:desafio_toro/modules/home/controller/home_controller.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeView extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    HomeController homeController = Provider.of<HomeController>(context);

    homeController.initListen();

    return Scaffold(
      body: Column(),
    );
  }
}
