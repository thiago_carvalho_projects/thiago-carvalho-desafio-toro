import 'dart:async';
import 'dart:convert';

import 'package:desafio_toro/modules/home/models/stocks_model.dart';
import 'package:web_socket_channel/io.dart';

class HomeController {
  IOWebSocketChannel channel;

  HomeController() {
    initListen();
  }

  StreamController<List<StocksModel>> topFive =
      StreamController<List<StocksModel>>();

  StreamController<List<StocksModel>> lastFive =
      StreamController<List<StocksModel>>();

  initListen() {
    channel = IOWebSocketChannel.connect("ws://localhost:8080/quotes");

    channel.stream.listen((message) {
      StocksModel stocksModel = StocksModel.fromJson(json.decode(message));

      print(stocksModel);
    });
  }

  dispose() {
    topFive.close();
    lastFive.close();
  }
}
