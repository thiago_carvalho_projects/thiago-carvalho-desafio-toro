import 'package:desafio_toro/modules/home/view/home_view.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

import 'controller/home_controller.dart';

class HomePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    // HomeController();

    return Provider(
      create: (context) {
        return HomeController();
      },
      child: HomeView(),
    );
  }
}
