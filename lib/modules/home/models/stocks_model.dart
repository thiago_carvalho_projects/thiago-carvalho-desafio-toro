import 'package:flutter/foundation.dart';

class StocksModel {
  String code;
  double value;
  double timestamp;

  StocksModel({this.timestamp, @required this.code, @required this.value});

  factory StocksModel.fromJson(Map<String, dynamic> json) {
    return StocksModel(
        code: json.keys.first,
        value: json[json.keys.first],
        timestamp: json['timestamp']);
  }
}

// "{"MGLU3": 9.97, "timestamp": 1611001912.217761}"
