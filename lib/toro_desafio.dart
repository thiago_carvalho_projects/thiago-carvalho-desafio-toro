import 'package:desafio_toro/shared/common/constants/font_family.dart';
import 'package:desafio_toro/shared/common/constants/toro_colors.dart';
import 'package:desafio_toro/shared/common/routes/app_router.dart';
import 'package:desafio_toro/shared/common/routes/app_routes.dart';
import 'package:flutter/material.dart';

final GlobalKey<NavigatorState> navigator = GlobalKey<NavigatorState>();

class ToroDesafio extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorKey: navigator,
      onGenerateRoute: AppRouter.generateRoute,
      initialRoute: AppRoutes.initialPage,
      theme: ThemeData(
          fontFamily: fontFamily, primaryColor: ToroColors.primaryColor),
    );
  }
}
