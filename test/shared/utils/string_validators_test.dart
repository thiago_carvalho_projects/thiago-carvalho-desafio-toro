import 'package:desafio_toro/shared/common/utils/string_validators.dart';
import 'package:flutter_test/flutter_test.dart';

main() {
  group('Testing String validatos', () {
    test('Test an empty email', () {
      String emptyEmail = '';

      String validationMessage = emptyEmail.validateEmail();

      expect(validationMessage, 'O email digitado não é válido');
    });
  });
}
